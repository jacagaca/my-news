import React from "react";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import { Switch, Route, Router } from "react-router-dom";
import ArticleList from "./components/ArticleList";
import PageNotFound from "./components/PageNotFound";
import { createBrowserHistory } from "history";

const App = () => {
  const history = createBrowserHistory();

  return (
    <div className="app">
      <Navbar />
      <div className="container">
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={ArticleList} />
            <Route exact path="/404" component={PageNotFound} />
          </Switch>
        </Router>
      </div>
      <Footer />
    </div>
  );
};

export default App;
