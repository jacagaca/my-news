import React from "react";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import renderer from "react-test-renderer";
import App from "../App";
import { Provider } from "react-redux";

import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

Enzyme.configure({
  adapter: new Adapter(),
});

let store;

const state = {
  articles: {
    data: [
      {
        url: "urltest123",
        title: "titletest123",
        excerpt: "excerpttest123",
        date: "datetest123",
        thumb: "thumbtest123",
      },
    ],
    fetching: true,
  },
};

beforeEach(() => {
  store = mockStore(state);
});

it("renders component and matches snap", async () => {
  store = mockStore(state);

  const tree = renderer
    .create(
      <Provider store={store}>
        <App store={store} />
      </Provider>
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});
