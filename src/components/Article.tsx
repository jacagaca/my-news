import React from "react";

export interface IArticle {
  author: string;
  title: string;
  description: string;
  publishedAt: string;
  url: string;
  urlToImage: string;
}

const Article = ({
  title,
  author,
  publishedAt,
  urlToImage,
  description,
  url,
}: IArticle) => {
  return (
    <div className="w3-container w3-white w3-margin w3-padding-large  w3-animate-opacity">
      <div className="w3-center">
        <a href={url} style={{ textDecoration: "none" }}>
          <h3>{title}</h3>
        </a>
        <h5>
          {author}, <span className="w3-opacity">{publishedAt}</span>
        </h5>
      </div>

      <div className="w3-justify">
        <a href={url} style={{ textDecoration: "none" }}>
          <img
            src={urlToImage}
            aria-hidden
            alt="article image"
            style={{ width: "100%" }}
            className="w3-padding-16"
          />
        </a>
        <p>{description}</p>
        <p className="w3-left">
          <button className="w3-button w3-white w3-border">
            <b>
              <i className="fa fa-comment" /> Comments
            </b>
          </button>
        </p>
        <p className="w3-right">
          <a href={url}>
            <button className="w3-button w3-white w3-border">
              <b>Read More</b>
            </button>
          </a>
        </p>
      </div>
    </div>
  );
};

export default Article;
