import React from "react";

import ArticlesColumn from "../components/ArticlesColumn";
import renderer from "react-test-renderer";

const articles = [];

it("renders component and matches snap", async () => {
  const tree = renderer.create(<ArticlesColumn articles={articles} />).toJSON();
  expect(tree).toMatchSnapshot();
});
