import React from "react";

import Navbar from "../components/Navbar";
import renderer from "react-test-renderer";

it("renders component and matches snap", async () => {
  const tree = renderer.create(<Navbar />).toJSON();
  expect(tree).toMatchSnapshot();
});
