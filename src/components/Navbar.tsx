import React, { useEffect, useState } from "react";

const Navbar = () => {
  const [showHeader, setShowHeader] = useState(true);

  useEffect(() => {
    window.addEventListener("scroll", listenToScroll);

    return () => {
      window.removeEventListener("scroll", listenToScroll);
    };
  }, []);

  const listenToScroll = () => {
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;

    if (winScroll > 250 || winScroll === 0) {
      setShowHeader(true);
    } else if (winScroll < 250 && winScroll > 1) {
      setShowHeader(false);
    }
  };

  return (
    <div
      className="w3-bar w3-black w3-hide-small w3-animate-top"
      style={
        showHeader
          ? {
              position: "sticky",
              top: "0px",
              zIndex: 10,
              transition: "all  0.2s  ease-in-out",
              userSelect: "none",
              pointerEvents: "none",
              opacity: 1,
            }
          : {
              userSelect: "none",
              zIndex: -1,
              position: "sticky",
              top: "0px",
              opacity: 0,
              transition: "all  0.2s  ease-in-out",
            }
      }
    >
      <div>
        <a
          href="https://www.facebook.com"
          target="_blank"
          rel="noopener noreferrer"
          className="w3-bar-item w3-button"
        >
          <i className="fa fa-facebook-official" />
        </a>
        <a
          href="https://www.instagram.com"
          target="_blank"
          rel="noopener noreferrer"
          className="w3-bar-item w3-button"
        >
          <i className="fa fa-instagram" />
        </a>

        <a
          href="https://www.twitter.com"
          target="_blank"
          rel="noopener noreferrer"
          className="w3-bar-item w3-button"
        >
          <i className="fa fa-twitter" />
        </a>

        <span className="w3-right">
          <a
            href="#search"
            target="_blank"
            rel="noopener noreferrer"
            className="w3-bar-item w3-button"
          >
            <i className="fa fa-search" />
          </a>
          <a
            href="#user"
            target="_blank"
            rel="noopener noreferrer"
            className="w3-bar-item w3-button "
          >
            <i className="fa fa-user" />
          </a>
          <a
            href="#menu"
            target="_blank"
            rel="noopener noreferrer"
            className="w3-bar-item w3-button"
          >
            <i className="fa fa-bars" />
          </a>
        </span>
      </div>
    </div>
  );
};

export default Navbar;
