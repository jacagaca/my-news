import React from "react";

const source = ["OKO", "Wyborcza", "Gazeta", "Interia", "Więcej..."];
const categories = ["Biznes", "Rozrywka", "Ogólne", "Zdrowie", "Więcej..."];

const SideBar = () => {
  return (
    <div className="sidebar">
      <div className="w3-col l4">
        <div className="w3-white w3-margin w3-animate-left">
          <div className="w3-container  w3-black ">
            <h4>Źródło</h4>
          </div>
          <div className="w3-container w3-white ">
            <div>
              <p>
                <button
                  className="w3-tag w3-black  w3-small w3-margin-right"
                  style={{ cursor: "pointer", marginBottom: "5px" }}
                >
                  Wszystkie
                </button>

                {source.map((item) => (
                  <button
                    key={item}
                    className="w3-tag w3-light-grey w3-small w3-margin-right  w3-hover-grey"
                    style={{ cursor: "pointer", marginBottom: "5px" }}
                  >
                    {item}
                  </button>
                ))}
              </p>
            </div>
          </div>
        </div>
        <div className="w3-white w3-margin w3-animate-left">
          <div className="w3-container  w3-black">
            <h4>Kategorie</h4>
          </div>
          <div className="w3-container w3-white">
            <p>
              <button
                className="w3-tag w3-black  w3-small w3-margin-right "
                style={{ cursor: "pointer", marginBottom: "5px" }}
              >
                Wszystkie
              </button>
              {categories.map((item) => (
                <button
                  key={item}
                  className="w3-tag w3-light-grey w3-small w3-margin-right  w3-hover-grey"
                  style={{ cursor: "pointer", marginBottom: "5px" }}
                >
                  {item}
                </button>
              ))}
            </p>
          </div>
        </div>

        <div className="w3-white w3-margin  w3-animate-left">
          <div className="w3-container  w3-black">
            <h4>Subskrybuj</h4>
          </div>
          <div className="w3-container w3-white">
            <p>Wprowadź swój adres e-mail poniżej i bądź na bieżaco.</p>
            <p>
              <input
                className="w3-input w3-border"
                type="text"
                placeholder="Wprowadź e-mail"
                style={{ width: "100%" }}
              />
            </p>
            <p>
              <button
                type="button"
                className="w3-button-small w3-block w3-black"
              >
                Subskrybuj
              </button>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideBar;
