import React from "react";
import SideBar from "../components/SideBar";
import renderer from "react-test-renderer";

it("renders component and matches snap", async () => {
  const tree = renderer.create(<SideBar />).toJSON();
  expect(tree).toMatchSnapshot();
});
