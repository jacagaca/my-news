import React from "react";
import renderer from "react-test-renderer";
import Article from "../components/Article";

it("renders component and matches snap", async () => {
  const tree = renderer.create(<Article></Article>).toJSON();
  expect(tree).toMatchSnapshot();
});
