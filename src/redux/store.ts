import { applyMiddleware, createStore } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import reducer from "./reducers";

const middleware = applyMiddleware(thunk, logger);

const store = createStore(reducer, middleware);

export type RootState = ReturnType<typeof store.getState>;
export default store;
