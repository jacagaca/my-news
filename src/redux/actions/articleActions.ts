import axios from "axios";
import { Dispatch } from "react";

export function fetchArticles(ammountFrom: number, ammountTo: number) {
  return (dispatch: Dispatch<any>) => {
    const url = `http://localhost:8000/posts?_start=${ammountFrom}&_end=${ammountTo}`;

    axios
      .get(url)
      .then((response) => {
        dispatch({
          type: "FETCH_ARTICLES",
          payload: response.data,
        });
        setTimeout(() => {
          dispatch({
            type: "FETCH_ARTICLES_FULFILLED",
            payload: response.data,
          });
        }, 1000);
      })
      .catch((error) => {
        dispatch({ type: "FETCH_ARTICLES_REJECTED", payload: error });
      });
  };
}
