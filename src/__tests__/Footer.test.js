import React from "react";
import Footer from "../components/Footer";
import renderer from "react-test-renderer";

it("renders component and matches snap", async () => {
  const tree = renderer.create(<Footer />).toJSON();
  expect(tree).toMatchSnapshot();
});
