import React, { useEffect, useState } from "react";
import Article from "./Article";
import { connect, useDispatch } from "react-redux";
import { fetchArticles } from "../redux/actions/articleActions";

import { Dispatch } from "redux";
import store from "../redux/store";

import SideBar from "./SideBar";
import ArticlesColumn from "./ArticlesColumn";

export interface IArticle {
  author: string;
  title: string;
  description: string;
  publishedAt: string;
  url: string;
  urlToImage: string;
  excerpt: string;
  date: string;
  thumb: string;
}

export interface IArticles {
  articles: [
    {
      author: string;
      title: string;
      description: string;
      publishedAt: string;
      url: string;
      urlToImage: string;
      excerpt: string;
      date: string;
      thumb: string;
    }
  ];
}

export interface ArticleListProps {
  articles: [
    {
      author: string;
      title: string;
      description: string;
      publishedAt: string;
      url: string;
      urlToImage: string;
      excerpt: string;
      date: string;
      thumb: string;
    }
  ];
  fetching: boolean;
}

const stateMap = (store: any) => {
  return {
    fetching: store.articles.fetching,
    articles: store.articles.data,
  };
};

const ArticleList = ({ articles, fetching }: ArticleListProps) => {
  const dispatch = useDispatch();

  const [articlesFeched, setArticlesFeched] = useState<IArticle[]>([
    {
      author: "Loading...",
      title: "Loading...",
      description: "Loading...",
      publishedAt: "Loading...",
      url: "Loading...",
      urlToImage: "Loading...",
      excerpt: "Loading...",
      date: "Loading...",
      thumb: "Loading...",
    },
  ]);

  const [fetchRange, setFetchRange] = useState({ from: 10, to: 20 });

  useEffect(() => {
    setArticlesFeched([]);
    dispatch(fetchArticles(0, 10)) as typeof store.dispatch | Dispatch<any>;
  }, [dispatch]);

  useEffect(() => {
    if (articles.length) {
      setArticlesFeched((data) => {
        return [...data, ...articles];
      });
    }
  }, [articles]);

  const fetchData = async () => {
    await dispatch(fetchArticles(fetchRange.from, fetchRange.to));
    setFetchRange({ from: fetchRange.from + 10, to: fetchRange.to + 10 });
  };

  const articleNodes = articlesFeched.map(
    (article: IArticle, index: number) => {
      if (index % 2 !== 0) {
        return (
          <Article
            key={index}
            author={"Jacek Pluta"}
            title={article.title}
            description={article.excerpt}
            publishedAt={article.date}
            url={article.url}
            urlToImage={article.thumb}
          />
        );
      } else {
        return <div key={index}></div>;
      }
    }
  );

  const articleNodes2 = articlesFeched.map(
    (article: IArticle, index: number) => {
      if (index % 2 === 0) {
        return (
          <Article
            key={index}
            author={"Jacek Pluta"}
            title={article.title}
            description={article.excerpt}
            publishedAt={article.date}
            url={article.url}
            urlToImage={article.thumb}
          />
        );
      } else {
        return <div key={index}></div>;
      }
    }
  );

  return (
    <div>
      <div className="w3-content" style={{ maxWidth: "1600px" }}>
        <header className="w3-container w3-center w3-padding-48 w3-white">
          <h1 className="w3-xxxlarge w3-animate-opacity">
            <b style={{ width: "80%", zIndex: -1 }}>Moje Wiadomości</b>
          </h1>
          <h6 className="w3-animate-opacity">
            Najnowsze Informacje z <span className="w3-tag">Polski</span>
          </h6>
        </header>
      </div>
      <div
        className="w3-row w3-padding w3-border"
        style={{ marginBottom: "50px" }}
      >
        <SideBar></SideBar>
        {/* {data &&
          data.length > 0 &&
          data.map((item1, index1) => {
            return (
              <div className="w3-col l4 s12   w3-animate-opacity" key={item1.id}>
              <ArticlesColumn
                fetchData={fetchData}
                articles={articleNodes}
                column={1}
              ></ArticlesColumn>
            </div>
            );
          })} */}

        <div className="w3-col l4 s12   w3-animate-top">
          <ArticlesColumn
            fetchData={fetchData}
            articles={articleNodes}
            column={1}
          ></ArticlesColumn>
        </div>

        <div className="w3-col l4 s12  w3-animate-top">
          <ArticlesColumn
            fetchData={fetchData}
            articles={articleNodes2}
            column={2}
          ></ArticlesColumn>
        </div>
      </div>
    </div>
  );
};

export default connect(stateMap)(ArticleList);
