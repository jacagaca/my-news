import React from "react";

const Footer = () => {
  return (
    <footer
      className="w3-container w3-black w3-center"
      style={{ padding: "15px" }}
    >
      <span>Copyright © 2020. Jacek Pluta</span>
    </footer>
  );
};

export default Footer;
