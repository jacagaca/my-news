import React from "react";
import { TextBlock, RectShape } from "react-placeholder/lib/placeholders";

export const AwesomePlaceholder = (
  <div className="w3-container w3-white w3-margin w3-padding-large">
    <div className="my-awesome-placeholder">
      <TextBlock
        style={{
          marginTop: 10,
        }}
        rows={5}
        color="lightgrey"
      />

      <RectShape
        color="lightgrey"
        style={{ marginTop: 20, marginBottom: 20, width: "100%", height: 200 }}
      />
      <TextBlock
        style={{
          marginTop: 10,
        }}
        rows={4}
        color="lightgrey"
      />
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: 20,
        }}
      >
        <RectShape color="lightgrey" style={{ width: "25%", height: 50 }} />
        <RectShape color="lightgrey" style={{ width: "25%", height: 50 }} />
      </div>
    </div>
  </div>
);
