import React from "react";

const PageNotFound = () => {
  return <div className="w3-bar w3-black w3-hide-small">404</div>;
};

export default PageNotFound;
