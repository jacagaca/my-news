import { IArticles } from "../../components/ArticleList";

type Action =
  | { type: "FETCH_ARTICLES"; payload: IArticles }
  | { type: "FETCH_ARTICLES_REJECTED"; payload: IArticles }
  | { type: "FETCH_ARTICLES_FULFILLED"; payload: IArticles };

export default function reducer(
  state = {
    data: [],
    fetching: false,
    fetched: false,
    error: null,
  },
  action: Action
) {
  switch (action.type) {
    case "FETCH_ARTICLES": {
      return { ...state, fetching: true };
    }
    case "FETCH_ARTICLES_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        data: action.payload,
      };
    }
    case "FETCH_ARTICLES_REJECTED": {
      return { ...state, fetching: false, error: action.payload };
    }
    default:
      return state;
  }
}
