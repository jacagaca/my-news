import React from "react";
import PageNotFound from "../components/PageNotFound";
import renderer from "react-test-renderer";

it("renders component and matches snap", async () => {
  const tree = renderer.create(<PageNotFound />).toJSON();
  expect(tree).toMatchSnapshot();
});
