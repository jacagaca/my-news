import ReactPlaceholder from "react-placeholder";
import { AwesomePlaceholder } from "./AwesomePlaceholder";
import InfiniteScroll from "react-infinite-scroll-component";
import React from "react";

const fakeSkeletonArray = Array.from({ length: 5 });

interface ArticlesColumnProps {
  articles: JSX.Element[];
  fetchData: any;
  column: number;
}

const ArticlesColumn = ({
  articles,
  fetchData,
  column,
}: ArticlesColumnProps) => {
  return (
    <InfiniteScroll
      dataLength={articles.length / 2}
      next={fetchData}
      hasMore={articles.length !== 200}
      loader={fakeSkeletonArray.map((item, index) => {
        return (
          <ReactPlaceholder
            key={index}
            ready={false}
            customPlaceholder={AwesomePlaceholder}
            showLoadingAnimation
          >
            Loading...
          </ReactPlaceholder>
        );
      })}
      endMessage={
        column === 1 ? (
          <p
            style={{
              textAlign: "center",
            }}
          >
            <b>Brak starszych wiadomości</b>
          </p>
        ) : (
          <p></p>
        )
      }
    >
      {articles}
    </InfiniteScroll>
  );
};

export default ArticlesColumn;
